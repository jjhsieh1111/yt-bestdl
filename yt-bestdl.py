#!/usr/bin/python
import os
import pafy
import getopt
import time
import ast

from multiprocessing import Pool

# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')

def combineVideoAudio (video_path, audio_path, output_path):
    combine_cmd = "ffmpeg -y -i '%s' -i '%s' -c copy '%s'" % (audio_path, video_path, output_path)
    print combine_cmd
    exit_value = os.system(combine_cmd)

    os.remove(audio_path)
    os.remove(video_path)

def downloadBestQualityMp4(url_id):
    video = pafy.new(url_id)

    print "Download video title = " + video.title
    print "Video URL(or ID) = " + url_id

    output_path = video.title + ".mp4"

    # if output_path already exist, skip it
    if os.path.isfile (os.path.abspath(output_path)):
        print ("Skip existing file: %s" % output_path)
        return

    best_audio = video.getbestaudio("m4a")
    print "Audio bitrate:" + best_audio.bitrate + ", extension:" + best_audio.extension
    audio_path = best_audio.download(video.title + ".audio." + best_audio.extension)

    best_video = video.getbestvideo("mp4")
    print "Video resolution:" + best_video.resolution + ", extension:" + best_video.extension
    video_path = best_video.download(video.title + ".video." + best_video.extension)

    combineVideoAudio (video_path, audio_path, output_path)

def usage():
    print 'Usage: youtube-dl.py -v url_id [-p playlist_url] [-j jobs]'
    print '  -v    Download single video with video URL or ID'
    print '  -p    Download videos in playlist with playlist URL'
    print '  -j    Download videos in parallel jobs, default is 1'

def main(argv):
    url_id = ''
    playlist = ''
    jobs_str = ''
    jobs = 1
    video_list = list()

    try:
        opts, args = getopt.getopt(argv,"hv:p:j:",["url_id=","playlist=","jobs="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-v", "--url_id"):
            url_id = arg
        elif opt in ("-p", "--playlist"):
            playlist = arg
        elif opt in ("-j", "--jobs"):
            jobs_str = arg
            jobs = ast.literal_eval(jobs_str)

    if url_id == '' and playlist == '':
        usage()
        sys.exit()

    # Append video URL or ID to download list
    if url_id != '':
        video_list.append (url_id)

    # Get all videos in this playlist
    if playlist != '':
        playlist = pafy.get_playlist(playlist)
        print "Playlist: %s, %d videos" % (playlist['title'], len(playlist['items']))

        # Append each video ID to download list
        for item in playlist['items']:
            video_list.append (item['pafy'].videoid)

    # Download all videos in multiple processes
    print "Downloading with %d processe(s)" % jobs
    pool = Pool(jobs)  # start worker processes
    pool.map(downloadBestQualityMp4, video_list)

if __name__ == '__main__':
    main(sys.argv[1:])
