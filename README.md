# yt-bestdl
Download youtube video or all videos in a playlist

pafy is used, to install pafy:
https://github.com/mps-youtube/pafy



Usage:
  youtube-dl.py -v <url> -p <playlist_url>

Download single video:
  youtube-dl.py -v video-url

Download all videos in playlist:
  youtube-dl.py -p playlist-url
